﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.UI.WebControls;
using System.Linq;
using System.Drawing;
//using Microsoft.AspNet.WebApi.Client;

namespace salary.app
{
    public partial class Default : System.Web.UI.Page
    {
        HttpClient http = new HttpClient();

        public Default()
        {
            http.BaseAddress = new Uri("https://localhost:44398/api/");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        
        protected void btnFilterEmployeeCode_Click(object sender, EventArgs e)
        {
           
            if (txtFilterEmployeeCode.Text.Length == 0)
            {
                string msg = "<script language=\"javascript\">";
                msg += "alert('Debe ingresar el códio de empleado');";
                msg += "</script>";
                Response.Write(msg);
                return;
            }

            var listData = http.GetAsync(string.Format("Salary/{0}", txtFilterEmployeeCode.Text)).Result;                
            
            if (listData.IsSuccessStatusCode)
            {
                var showData = (listData.Content.ReadAsAsync<List<Salary>>().Result).OrderByDescending(x => x.Month).ToList();

                gridData.DataSource = showData;
                gridData.DataBind();
                NoEncontrado.Visible = false;

                if (!(showData.Count > 0))
                {
                    NoEncontrado.Visible = true;
                    return;
                }

                var data = (from s in showData select s).Take(3).ToList();

                bool isContinue = true;
                var consecutivos = new List<int>();
                int first = data.First().Month;

                decimal bonoSalarial = 0;

                for (int index = 0; (index < data.Count) && isContinue; index++)
                {
                    if ((first - index) != data[index].Month)
                    {
                        isContinue = false;
                    }
                    else
                    {
                        consecutivos.Add(data[index].Id);
                        bonoSalarial += data[index].BaseSalary;
                    }
                }

                bonoSalarial /= 3;

                bool encontrado;
                for (int a = 0; a < consecutivos.Count; a++)
                {
                    encontrado = false;
                    for (int indice = 0; (indice < gridData.Rows.Count) && !encontrado; indice++)
                    {
                        int id = 0;
                        bool success = int.TryParse(gridData.DataKeys[indice].Value.ToString(), out id);
                        if (success)
                        {
                            if (id == consecutivos[a])
                            {
                                encontrado = true;
                                gridData.Rows[indice].Style.Add(System.Web.UI.HtmlTextWriterStyle.BackgroundColor, "yellow");
                            }
                        }
                    }
                }
                GridViewRow row = new GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal);

                TableCell bono = new TableHeaderCell();
                //totals.ColumnSpan = gridData.Columns.Count - 9;
                bono.ColumnSpan = 8;
                bono.Text = "Bono";
                row.Cells.Add(bono);

                TableCell valor = new TableHeaderCell();
                //totals.ColumnSpan = gridData.Columns.Count - 9;
                valor.ColumnSpan = 9;
                valor.Text = bonoSalarial.ToString();
                row.Cells.Add(valor);

                Table t = gridData.Controls[0] as Table;
                if(t != null)
                {
                    t.Rows.AddAt(gridData.Rows.Count + 1, row);
                }
            }
        }

    }
}