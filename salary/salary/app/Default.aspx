﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="salary.app.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <h2>Filtre un empleado</h2>
            <div class="col-2">
                <asp:TextBox ID="txtFilterEmployeeCode" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-2">
                <asp:Button ID="btnFilterEmployeeCode" CssClass="btn btn-primary" OnClick="btnFilterEmployeeCode_Click" runat="server" Text="Filtrar" />
            </div>
        </div>
        <br />
        <div class="row">
            <asp:GridView ID="gridData" CssClass="table table-responsive" runat="server"
                AutoGenerateColumns="False" DataKeyNames="Id">

                <Columns>
                    <asp:BoundField DataField="EmployeeCode" HeaderText="Employee Code" SortExpression="EmployeeCode" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
                    <asp:BoundField DataField="Year" HeaderText="Year" SortExpression="Year" />
                    <asp:BoundField DataField="Month" HeaderText="Month" SortExpression="Month" />
                    <asp:BoundField DataField="Office.Nombre" HeaderText="Office" SortExpression="Office.Nombre" />
                    <asp:BoundField DataField="Position.Nombre" HeaderText="Position" SortExpression="Position.Nombre" />
                    <asp:BoundField DataField="Division.Nombre" HeaderText="Division" SortExpression="Division.Nombre" />
                    <asp:BoundField DataField="BaseSalary" HeaderText="Base Salary" SortExpression="BaseSalary" />
                    <asp:BoundField DataField="IdentificationNumber" HeaderText="Identification Number" SortExpression="IdentificationNumber" /> 
                    <asp:BoundField DataField="BeginDate" HeaderText="Begin Date" SortExpression="BeginDate" />
                </Columns>

            </asp:GridView>

            <div ID="NoEncontrado" visible="false" runat="server"> Empleado no encontrado</div>

        </div>
    </div>
</asp:Content>
