﻿using salary.app.Entities;
using System;

namespace salary.app
{
    public class Salary
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int OfficeId { get; set; }
        public string EmployeeCode { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime BirthDay { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Comission { get; set; }
        public decimal Contributions { get; set; }

        public Office Office { get; set; }
        public Division Division { get; set; }
        public Position Position { get; set; }
    }
}