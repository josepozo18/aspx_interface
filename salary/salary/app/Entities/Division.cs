﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace salary.app.Entities
{
    public class Division
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}